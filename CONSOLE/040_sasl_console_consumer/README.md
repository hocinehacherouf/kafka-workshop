#### This exercise aims at consuming records using the Kafka console consumer using SASL authentication and ACLs

#### Start the Kafka cluster :
If your Docker containers of the previous the exercise 05_sasl_producer are stopped, start them  
```docker-compose start```

The cluster is composed of 1 zookeeper and 3 brokers  
The docker-compose file contains also a base container that allows to run Kafka commands  
The cluster is configured to create topics automatically on the first production of records  
The cluster is configured to use SASL_PLAINTEXT protocol with the SCRAM-SHA-512 SASL mechanism  
```advertised.listeners=SASL_PLAINTEXT://kafka2:29092```  
```listeners=SASL_PLAINTEXT://0.0.0.0:29092```  
```security.inter.broker.protocol=SASL_PLAINTEXT```  
```sasl.enabled.mechanisms=SCRAM-SHA-512```  
```sasl.mechanism.inter.broker.protocol=SCRAM-SHA-512```  
Each broker is started with KAFKA_OPTS options:```-Djava.security.auth.login.config=/etc/kafka/secrets/broker_jaas.conf```  
Each zookeeper is started with KAFKA_OPTS options: ```-Djava.security.auth.login.config=/etc/kafka/secrets/zookeeper_jaas.conf```  
 
#### Interact with Kafka cluster
Get into the base container  
```docker exec -it base bash```
	
#### Create users
Using the kafka-configs command, create the admin user withe admin-secret as password  
```kafka-configs --zookeeper zookeeper:2181 --alter --add-config 'SCRAM-SHA-512=[password=admin-secret]' --entity-type users --entity-name admin```  
Using the kafka-configs command, create an application user app-user withe password as app-password  
```kafka-configs --zookeeper zookeeper:2181 --alter --add-config 'SCRAM-SHA-512=[password=app-password]' --entity-type users --entity-name app-user```  
	
#### Create a topic
Create a topic named adminSaslTopic using the kafka-topics command, replicated 3 times and with 3 partitions with the admin user  
```kafka-topics --create --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic adminSaslTopic --replication-factor 3 --partitions 3```  
Note the timeout, you must provide authentication to the cluster in order to make actions on it   
```kafka-topics --create --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic adminSaslTopic --replication-factor 3 --partitions 3 --command-config /etc/kafka/sasl/admin-sasl.properties```  
Create a topic named userSaslTopic using the kafka-topics command, replicated 3 times and with 3 partitions with the app-user user  
```kafka-topics --create --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic userSaslTopic --replication-factor 3 --partitions 3 --command-config /etc/kafka/sasl/user-sasl.properties```  
Note the not authorized exception:  org.apache.kafka.common.errors.TopicAuthorizationException: Not authorized to access topics  
You must provide ACLs authorization in order to create topics 

#### Adding ACL
Acls to create topics must be added to the app-user user  
Using the kafka-acl command, add the CREATE topic Acls to the app-user user  
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --add --allow-principal User:app-user --operation CREATE --cluster ```   
List the existing ACLs           
```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --list```  
Retry to create a topic named userSaslTopic using the kafka-topics command, replicated 3 times and with 3 partitions with the app-user user  
```kafka-topics --create --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic userSaslTopic --replication-factor 3 --partitions 3 --command-config /etc/kafka/sasl/user-sasl.properties```  

#### Produce records
Using the Kafka console producer, send records to the topic userSaslTopic  
```kafka-console-producer --broker-list kafka1:19092,kafka2:29092,kafka3:39092 --topic userSaslTopic --producer.config /etc/kafka/sasl/user-sasl.properties```  
Note the org.apache.kafka.common.errors.TopicAuthorizationException: Not authorized to access topics: [userSaslTopic]  
You must provide ACLs authorization in order to write to topics to the app-user  
 ```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --add --allow-principal User:app-user --operation WRITE --topic userSaslTopic```   
Retry to send records to the topic userSaslTopic  
```kafka-console-producer --broker-list kafka1:19092,kafka2:29092,kafka3:39092 --topic userSaslTopic --producer.config /etc/kafka/sasl/user-sasl.properties```  
Provide Read rights to all users belonging to all groups to consume from all topics  
 ```kafka-acls --authorizer kafka.security.auth.SimpleAclAuthorizer --authorizer-properties zookeeper.connect=zookeeper:2181 --add --allow-principal User:'*' --group '*' --operation READ --topic '*'```   
Using the --consumer.config option, consume records from  userSaslTopic with app-user  
```kafka-console-consumer --bootstrap-server kafka1:19092,kafka2:29092,kafka3:39092 --topic userSaslTopic --group app-group --from-beginning --consumer.config /etc/kafka/sasl/user-sasl.properties```  
Records are consumed successfully
