CREATE STREAM workshop_info (id int, description varchar) WITH (kafka_topic='workshop_info_topic', partitions=3, value_format = 'avro');

DESCRIBE workshop_info;

INSERT INTO workshop_info (id, description) VALUES (1, 'description of 1');
INSERT INTO workshop_info (id, description) VALUES (2, 'description of 2');
INSERT INTO workshop_info (id, description) VALUES (3, 'description of 3');
INSERT INTO workshop_info (id, description) VALUES (4, 'description of 4');
INSERT INTO workshop_info (id, description) VALUES (5, 'description of 5');
INSERT INTO workshop_info (id, description) VALUES (6, 'description of 6');

SET 'auto.offset.reset' = 'earliest';

SELECT ROWKEY, id, description FROM workshop_info;

CTRL+C

CREATE STREAM  workshop_info_rekeyed AS SELECT * FROM workshop_info PARTITION BY id;

SELECT ROWKEY, id, description FROM workshop_info;

SHOW topics;

PRINT 'WORKSHOP_INFO_REKEYED';