#### This exercise aims at filtering a stream, merging a stream and a table with KSQL

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

####Interact with the KSQL Client
Get into the ksql client container  
``docker exec -it ksql-cli ksql http://ksql-server:8088``

####Filter a STREAM  
Select datas from workshop_stats_transform where temperature is higher or equals to 80  
``SELECT * FROM workshop_stats_transform WHERE ...``  
Create a new stream from this select  
Name the stream workshop_stats_transform_filtered  
Name the topic workshop_stats_transform_filtered_topic  
Set the key with id  
Set the format with AVRO  
``CREATE STREAM workshop_stats_transform_filtered...``  
Show details of the created stream  
``DESCRIBE workshop_stats_transform_filtered;``  
Print out the contents of the workshop_stats_transform_filtered_topic    
``PRINT ...``  
Note the content has been filtered  

####Create a TABLE  
Create a table from the workshop_INFO_REKEYED topic  
Name the table workshop_info_table  
Name the topic workshop_info_table_topic  
Set the key with id  
Set the format with AVRO  
``CREATE TABLE workshop_info_table...``  
Show details of the created table  
``DESCRIBE workshop_info_table;``  
Select the content of workshop_info_table table  
``SELECT * FROM workshop_info_table;``  
  

####Update datas in the TABLE   
``INSERT INTO workshop_info (id, description) VALUES (111111, 'description updated of 1');``  
``INSERT INTO workshop_info (id, description) VALUES (333333, 'description updated of 3');``  
``INSERT INTO workshop_info (id, description) VALUES (111111, 'description updated again of 1');``  
``INSERT INTO workshop_info (id, description) VALUES (222222, 'description updated of 2');``  

Select the content of workshop_info  
``SELECT * FROM workshop_info;``  
Select the content of workshop_info_rekeyed  
``SELECT * FROM workshop_info_rekeyed;``  
Select the content of workshop_info  
``SELECT * FROM workshop_info_table;``  

####Set the auto offset resset to earliest
In order to read events from beginning, set the auto.offset.reset to earliest  
``SET 'auto.offset.reset' = 'earliest';``  

####Merge the filtered STREAM and the TABLE
Using the LEFT JOIN clause, merge the workshop_stats_transform_filtered stream and the workshop_info_table table  
``SELECT workshop_stats_transform_filtered.id ... FROM workshop_stats_transform_filtered LEFT JOIN ...``  
Note that there is nothing return from workshop_info_table, this is because the table have been filled later  
Insert more data in the workshop_stats stream  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '110::120 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::80 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (444444, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::110 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (666666, '60::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '90::90 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::160 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '10::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '150::100 bar', '4season');``  
Using the LEFT JOIN clause, merge again the workshop_stats_transform_filtered stream and the workshop_info_table table  
``SELECT workshop_stats_transform_filtered.id ... FROM workshop_stats_transform_filtered LEFT JOIN ...``  
This time, all properties are filled  
Create a new stream from this select  
Name the stream merge_stream  
Name the topic merge_stream_topic with 3 partitions  
Set the format with AVRO  
``CREATE STREAM ...``  

####Print out the existing topics
Check that the merge_stream_topic has been created  
``SHOW topics``  

####Print out the contents of the output streams
Print out the contents of the merge_stream_topic  
``PRINT 'merge_stream_topic'``  