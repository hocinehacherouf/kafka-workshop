CREATE STREAM workshop_stats_window (id int, stats int, type varchar, timestamp varchar)
    WITH (kafka_topic='workshop_stats_window_topic',
          key='id',
          timestamp='timestamp',
          timestamp_format='yyyy-MM-dd HH:mm:ss',
          partitions=3,
          value_format='avro');

DESCRIBE workshop_stats_window;

INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 12, '4season', '2019-07-09 01:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 10, '4season', '2019-07-09 05:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 90, '4season', '2019-07-09 07:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 80, 'summer', '2019-07-09 09:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 50, 'summer', '2019-07-09 08:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 60, 'winter', '2019-07-09 12:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 90, 'winter', '2019-07-09 15:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 80, 'winter', '2019-07-09 22:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 150, '4season', '2019-07-09 05:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 120, '4season', '2019-07-09 02:00:00');
INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (5, 10, 'summer', '2019-07-09 18:00:00');

SELECT id, COUNT(*) AS stats_count, WINDOWSTART() AS window_start, WINDOWEND() AS window_end
        FROM workshop_stats_window WINDOW TUMBLING (SIZE 6 HOURS) GROUP BY id;

CREATE TABLE stats_count WITH (kafka_topic='stats_count') AS
    SELECT id, COUNT(*) AS stats_count, WINDOWSTART() AS window_start, WINDOWEND() AS window_end
            FROM workshop_stats_window WINDOW TUMBLING (SIZE 6 HOURS) GROUP BY id;

SELECT id, stats_count,
       TIMESTAMPTOSTRING(window_start, 'yyy-MM-dd HH:mm:ss'),
       TIMESTAMPTOSTRING(window_end, 'yyy-MM-dd HH:mm:ss')
FROM stats_count;

