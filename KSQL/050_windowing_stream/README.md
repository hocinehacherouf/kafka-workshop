#### This exercise aims at count elements from a stream using Tumbling windowing

#### Start the Kafka cluster
Start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

####Interact with the KSQL Client
Get into the ksql client container    
``docker exec -it ksql-cli ksql http://ksql-server:8088``


####Create a STREAM
Create a stream that represents stats from a stats topic  
Name the topic workshop_stats_window_topic, it must have 3 partitions  
Name the stream workshop_stats_window with 4 columns: id (int), stats (varchar), type (varchar), timestamp (varchar)  
Set the key with id  
Set the timestamp with timestamp column  
Set the timestamp_format to 'yyyy-MM-dd HH:mm:ss'  
Set the format with AVRO  
``CREATE STREAM workshop_stats_window...``  
Show details of the created stream  
``DESCRIBE workshop_stats_window;``  

####``INSERT values into the stream
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 12, '4season', '2019-07-09 01:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 10, '4season', '2019-07-09 05:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (1, 90, '4season', '2019-07-09 07:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 80, 'summer', '2019-07-09 09:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (2, 50, 'summer', '2019-07-09 08:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 60, 'winter', '2019-07-09 12:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 90, 'winter', '2019-07-09 15:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (3, 80, 'winter', '2019-07-09 22:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 150, '4season', '2019-07-09 05:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (4, 120, '4season', '2019-07-09 02:00:00');``  
``INSERT INTO workshop_stats_window (id, stats, type, timestamp) VALUES (5, 10, 'summer', '2019-07-09 18:00:00');``  

####Use Tumbling window  
Compute how many stats were received in 6 hour tumbling intervals  
To do that, aggregate the stats grouped by the id  
Captures the window start and end times with WINDOWSTART() and WINDOWEND() functions in the select clause  
``SELECT id, COUNT(*) AS rating_count,...``  
Create a table from this select statement. This table will be continously populated  
``CREATE TABLE stats_count...`` 
Select the content of stats_count table  
Format the timestamp to human readable form using the TIMESTAMPTOSTRING function   
``SELECT stats_count, ... FROM stats_count;`` 
  

####Update datas in the TABLE   
``INSERT INTO workshop_info (id, description) VALUES (111111, 'description updated of 1');``  
``INSERT INTO workshop_info (id, description) VALUES (333333, 'description updated of 3');``  
``INSERT INTO workshop_info (id, description) VALUES (111111, 'description updated again of 1');``  
``INSERT INTO workshop_info (id, description) VALUES (222222, 'description updated of 2');``  

Select the content of workshop_info  
``SELECT * FROM workshop_info;``   
Select the content of workshop_info_rekeyed  
``SELECT * FROM workshop_info_rekeyed;``  
Select the content of workshop_info  
``SELECT * FROM workshop_info_table;``  

####Set the auto offset resset to earliest
In order to read events from beginning, set the auto.offset.reset to earliest  
``SET 'auto.offset.reset' = 'earliest';``

####Merge the filtered STREAM and the TABLE
Using the LEFT JOIN clause, merge the workshop_stats_transform_filtered stream and the workshop_info_table table  
``SELECT workshop_stats_transform_filtered.id ... FROM workshop_stats_transform_filtered LEFT JOIN ...`` 
Note that there is nothing return from workshop_info_table, this is because the table have been filled later  
INSERT more data in the workshop_stats stream  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '110::120 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::80 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (444444, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::110 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (666666, '60::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '90::90 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::160 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (555555, '150::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (111111, '120::100 bar', 'winter');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '10::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '90::100 bar', '4season');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (222222, '80::100 bar', 'summer');``  
``INSERT INTO workshop_stats (id, stats, type) VALUES (333333, '150::100 bar', '4season');``  
Using the LEFT JOIN clause, merge again the workshop_stats_transform_filtered stream and the workshop_info_table table  
``SELECT workshop_stats_transform_filtered.id ... FROM workshop_stats_transform_filtered LEFT JOIN ...``  
This time, all properties are filled  
Create a new stream from this select  
Name the stream merge_stream  
Name the topic merge_stream_topic with 3 partitions  
Set the format with AVRO  
``CREATE STREAM ...``  

####Print out the existing topics
Check that the merge_stream_topic has been created  
``SHOW topics``  

####Print out the contents of the output streams
Print out the contents of the merge_stream_topic  
``PRINT 'merge_stream_topic'`` 