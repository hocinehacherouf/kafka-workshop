package partial.com.kafka.workshop;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application launcher
 *
 */
@SpringBootApplication
public class SaslConsumerApplication {

	public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SaslConsumerApplication.class);
        application.run(args);
    }
	
}
