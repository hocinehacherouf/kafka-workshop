package solution.com.kafka.workshop;

import com.michelin.workshop.avro.WorkshopRef;
import com.michelin.workshop.avro.WorkshopConsolidated;
import com.michelin.workshop.avro.WorkshopStats;
import org.apache.kafka.streams.kstream.ValueJoiner;

public class DatasJoiner implements ValueJoiner<WorkshopStats, WorkshopRef, WorkshopConsolidated> {

    @Override
    public WorkshopConsolidated apply(WorkshopStats datas, WorkshopRef ref) {
        return WorkshopConsolidated.newBuilder()
                .setIdent(ref.getIdent())
                .setName(ref.getName())
                .setDescription(ref.getDescription())
                .setTemperature(datas.getTemperature())
                .setPressure(datas.getPressure())
                .build();
    }
}
