package streams;

import com.michelin.workshop.avro.WorkshopConsolidated;
import com.michelin.workshop.avro.WorkshopRef;
import com.michelin.workshop.avro.WorkshopStats;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.Before;
import solution.com.kafka.workshop.TestingStream;

import java.util.HashMap;
import java.util.Map;

public class MockedKafkaClusterTestUtils {


    protected TopologyTestDriver testDriver;

    protected final String refTopic = "TIRE-TOPIC";
    protected final String rekeyedRefTopic = "REKEYED-TIRE-TOPIC";
    protected final String statsTopic = "DATAS-TOPIC";
    protected final String consolidatedTopic = "CONSOLIDATED-TOPIC";
    protected final String alertTopic = "ALERT-TOPIC";
    private final String DUMMY_URL = "http://dummy";

    protected final Deserializer<String> stringDeserializer =  Serdes.String().deserializer();
    protected SpecificAvroSerde<WorkshopRef> refAvroSerde = makeSerializer();
    protected SpecificAvroSerde<WorkshopRef> rekeyedRefAvroSerde = makeSerializer();
    protected SpecificAvroSerde<WorkshopStats> statsAvroSerde = makeSerializer();
    protected SpecificAvroSerde<WorkshopConsolidated> consolidatedDatasAvroSerde = makeSerializer();

    private SpecificAvroSerde makeSerializer() {
        MockSchemaRegistryClient client = new MockSchemaRegistryClient();
        SpecificAvroSerde serde = new SpecificAvroSerde(client);
        Map<String, String> config = new HashMap<>();
        config.put("schema.registry.url", DUMMY_URL);
        serde.configure(config, false);
        return serde;
    }


    @Before
    public void testSetup() {
        TestingStream kafkaStreamBuilder = new TestingStream(
                refTopic,
                statsTopic,
                rekeyedRefTopic,
                consolidatedTopic,
                alertTopic,
                DUMMY_URL,
                DUMMY_URL,
                "app-unit-test",
                "client-unit-test",
                "earliest"
        );

        // Workaround for setting serdes with MockSchemaRegistryClient
        kafkaStreamBuilder.setRefAvroSerde(refAvroSerde);
        kafkaStreamBuilder.setStatsAvroSerde(statsAvroSerde);
        kafkaStreamBuilder.setConsolidatedDatasAvroSerde(consolidatedDatasAvroSerde);
        kafkaStreamBuilder.setRekeyedRefAvroSerde(rekeyedRefAvroSerde);

        // Create an instance of TopologyTestDriver with the topology to test
        testDriver = new TopologyTestDriver(kafkaStreamBuilder.buildTopology(), kafkaStreamBuilder.configureStream());

    }

    //consumer factory
    protected final ConsumerRecordFactory<String, WorkshopRef> refRecordFactory =
            new ConsumerRecordFactory<>(
                    refTopic,
                    Serdes.String().serializer(),
                    refAvroSerde.serializer()
            );

    protected final ConsumerRecordFactory<String, WorkshopStats> statsRecordFactory =
            new ConsumerRecordFactory<>(
                    statsTopic,
                    Serdes.String().serializer(),
                    statsAvroSerde.serializer()
            );

}
