#### This exercise aims at writing a KSTREAM java application using interactive queries
The objective of this exercise is to:
- read records from statestore-topic-in topic with KafkaStream application  
  Use three different steps to demonstrate interactive queries  
  Step1: use a local state store for multiple instances  
  Step2: use a local state store for multiple instances using a RPC layer  
  Step3: use a local state store that contains all topic values  
  The topic contains String key / WorkshopValue value pairs  
  Query state store created during the three steps  
      
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Produce example records  
Use the com.kafka.workshop.init.producer.StateStoreProducerApplication class to produce records to statestore-topic-in topic  
It produces 10 WorkshopValue objects  

#### Step 1  
Configure the stream topology following the TODO and Step 1 instructions in the partial.com.kafka.workshop.StateStore  
Query the state store following the TODO instructions in the partial.com.kafka.workshop.controller.RestController java classes
Start an instance of the application on the 9080 port  
Use you favorite browser or http client to query the state store  
```http://localhost:9080/stateStore/allByInstance``` 
Get a key in the reference property  
```http://localhost:9080/stateStore/allByInstance/key?key=your_key```  
Note that all values that have been produced are displayed  

Start another instance of the application on the 9081 port  
Use you favorite browser or http client to query the state store of the two running applications   
```http://localhost:9080/stateStore/allByInstance``` 
```http://localhost:9081/stateStore/allByInstance```  
Note that each value is only present on one application instance   

#### Step 2 add RPC layer  
Stop all running applications  
Configure the stream topology following the TODO and Step 2 instructions in the partial.com.kafka.workshop.StateStore  
Start an instance of the application on the 9080 port  
Use you favorite browser or http client to query the state store  
```http://localhost:9080/stateStore/all``` 
Note that all values that have been produced are displayed  

Start another instance of the application on the 9081 port  
Use you favorite browser or http client to query the state store of the two running applications   
```http://localhost:9080/stateStore/all``` 
```http://localhost:9081/stateStore/all```  
Note that each application instance displays all topic values  using the RPC layer  
The called instance send an http request to the other instance to query its state store  

#### Step 3 GlobalKTable  
Stop all running applications 
Configure the stream topology following the TODO and Step 3 instructions in the partial.com.kafka.workshop.StateStore  
Start an instance of the application on the 9080 port  
Use you favorite browser or http client to query the state store  
```http://localhost:9080/stateStore/global/all``` 
Note that all values that have been produced are displayed  

Start another instance of the application on the 9081 port  
Use you favorite browser or http client to query the state store of the two running applications   
```http://localhost:9080/stateStore/global/all```  
```http://localhost:9081/stateStore/global/all```  
Note that each application instance displays all topic values but using the RPC layer  
Each instance contains all topic values in its statestore  
