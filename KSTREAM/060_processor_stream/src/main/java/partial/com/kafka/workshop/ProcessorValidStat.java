package partial.com.kafka.workshop;

import com.kafka.workshop.avro.StatValid;
import com.kafka.workshop.avro.WorkshopStat;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.springframework.stereotype.Component;

@Component
public class ProcessorValidStat implements Processor<String, WorkshopStat> {

     private ProcessorContext context;

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public void process(String key, WorkshopStat value) {
        String header = new String(context.headers().lastHeader("validityDate").value());
        StatValid stat = StatValid.newBuilder()
                .setId(value.getId())
                .setRef(value.getRef())
                .setStat(value.getStat())
                .setValidityDate(header)
                .build();
        context.forward(key, stat);
    }

    @Override
    public void close() {

    }

}
