package com.kafka.workshop.init.producer;

import avro.shaded.com.google.common.collect.Lists;
import com.kafka.workshop.avro.WorkshopRef;
import com.kafka.workshop.avro.WorkshopStat;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StreamProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StreamProducer.class);

    private static final String topic_stream_in = "merge-stat-topic";
    private static final String topic_table_in = "merge-ref-topic";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopStat> producer = new KafkaProducer<>(props);
        Producer<String, WorkshopRef> producerRef = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopStat workshopStat;

            for(int i=0; i<100; i++) {

                key = getAlphaNumericString(3);
                workshopStat = WorkshopStat.newBuilder()
                        .setId(i)
                        .setReference(getRandomReference())
                        .setStat(getRandomStat() + "::" + getRandomStat())
                        .build();

                ProducerRecord<String, WorkshopStat> record = new ProducerRecord<>(topic_stream_in, key, workshopStat);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }
        } finally {
            producer.close();
        }

            try {

                String key;
                WorkshopRef workshopRef;

                for(int i=0; i<6; i++) {
                    key = getReference(i);
                    workshopRef = WorkshopRef.newBuilder()
                            .setReference(key)
                            .setDescription(String.format("description %s", key))
                            .build();

                    ProducerRecord<String, WorkshopRef> record = new ProducerRecord<>(topic_table_in, key, workshopRef);
                    producerRef.send(record, new Callback() {
                        public void onCompletion(RecordMetadata metadata, Exception e) {
                            if (e != null) {
                                logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                            } else {
                                logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                            }
                        }
                    });
                }
            } finally {
                producerRef.close();
            }
    }

    public static String getRandomReference() {
        Random rand = new Random();
        List<String> givenList = Lists.newArrayList("111111", "222222", "333333", "444444", "555555", "666666");
        int randomIndex = rand.nextInt(givenList.size());
        String reference = givenList.get(randomIndex);
        return  reference;

    }

    private static String getRandomStat() {
        Random rand = new Random();
        List<String> givenList = Lists.newArrayList("50", "60", "70", "80", "90", "100", "110", "120", "130");

        int randomIndex = rand.nextInt(givenList.size());
        String stat = givenList.get(randomIndex);
        return  stat;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private static String getReference(int index) {
        List<String> givenList = Lists.newArrayList("111111", "222222", "333333", "444444", "555555", "666666");
        String reference = givenList.get(index);
        return  reference;

    }


}
