#### This exercise aims at writing a KSTREAM java application consuming String records and produce Avro records

The objective of this exercise is to:
- read records from transform-topic-in topic with KafkaStream application  
  The topic contains String key / String value pair  
  Each record represents a line a WorkshopValue object formatted as: id::longDescription::shortDescription::reference  
- transform each record into Long key / AVRO WorkshopValue pair  
- produce the new record into transform-topic-in topic
    
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  


#### Configure the KSTREAM topology
Configure the stream topology following the TODO instructions in the partial.com.kafka.workshop.TransformStream java class

####Start the Stream application
Start the application  

#### Produce example records
Use the com.workshop.kafka.com.kafka.workshop.init.producer.TransformStreamProducerApplication class to produce records to transform-topic-in  
The produce records are String values representing a WorkshopValue formatted as: id::longDescription::shortDescription::reference  

#### Consume example records
Use the com.workshop.kafka.init.consumer.TransformStreamConsumerApplication class to consumer records from transform-topic-in     
Check the transformation is made correctly    
