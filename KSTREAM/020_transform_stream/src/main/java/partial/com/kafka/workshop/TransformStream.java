package partial.com.kafka.workshop;


import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class TransformStream implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(TransformStream.class);

    private final String topic_in = "transform-topic-in";
    private final String topic_out = "transform-topic-out";

    @Override
    public void run(ApplicationArguments args) {

        createTopics(topic_in);
        createTopics(topic_out);
        try {
            // Create KafkaStreams instance
            KafkaStreams streams = new KafkaStreams(getTopology(), configureStream());

            // Start the stream
            streams.start();

            // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    streams.close();
                } catch (Exception e) {
                    logger.warn("Error while trying to close stream", e.getMessage(), e);
                }
            }));
        } catch (Exception e) {
            logger.error("Cannot start stream processor", e.getMessage(), e);
        }
    }

    /**
     * configure the stream
     *
     * @return
     */
    private Properties configureStream() {
        Properties streamsConfiguration = new Properties();
        // Set the bootstrap servers using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Set schema registry url using StreamsConfig class
        streamsConfiguration.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        // Set the application Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "transform-app-id");
        // Set the client Id using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, "transform-cli-id");
        // Set the default key SerialiZation / Deserialization using StreamsConfig class
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // Set the auto reset offset to the earliest commited offset using the ConsumerConfig class
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return streamsConfiguration;
    }

    /**
     * build the stream topology
     *
     * @return
     */
    private Topology getTopology() {
        // TODO Create StreamsBuilder instance
        final StreamsBuilder builder = null;

        /*
            TODO Create a KStream instance from the StreamBuilder instance
                 Consume topic_in by specifying the Serdes for key and value with Consumed.with() method
        */
        KStream<String, String> stream = null;
        /*
            TODO Re-key the stream using the map method:
                                    set the key, use the extracKey method
                                    set value with a workshopValue object, use the convertWorkshop method
                 Set it to a new KStream object
         */
        KStream<Long, WorkshopValue> transformedStream = null;
        /*
            TODO Produce the new stream to topic_out by specifying the Serdes for key and value with Produces.with() method
        */


        return builder.build();

    }

    /**
     * Get the SpecificAvroSerde for the WorkshopValue object
     *
     * @return
     */
    private SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde() {
        // TODO Create an instance of a SpecificAvroSerde<WorkshopValue>
        SpecificAvroSerde<WorkshopValue> workshopValueAvroSerde = null;
        /*
            TODO Create a Map<String, String> instance, put the key value pair: schema.registry.url / registry url
                 Use the getSchemaRegistryAvroConf() method to get a Map instance containing a reference to the schela registry url
                 Use the configure method of the SpecificAvroSerde instance to add the Map instance, warning: this is a value
        */


        return workshopValueAvroSerde;
    }


    /**
     * @return
     */
    private Map<String, String> getSchemaRegistryAvroConf() {
        final HashMap<String, String> serdeConfig = new HashMap<>();
        /*
            TODO Add a key value pair: schema.registry.url / registry url
                 Use the AbstractKafkaAvroSerDeConfig class
        */
        return serdeConfig;
    }


    /**
     * Convert a String value object to a WorkshopValue object
     *
     * @param value
     * @return
     */
    public static WorkshopValue convertWorkshop(String value) {
        String[] valueArray = StringUtils.split(value, "::");
        Long id = Long.parseLong(valueArray[0]);
        String longDesc = valueArray[1];
        String shortDesc = valueArray[2];
        String reference = valueArray[3];
        // TODO Create a WorkshopValue object
        WorkshopValue workshopValue = null;
        return workshopValue;
    }

    /**
     * Extract key as long from String value
     * @param value
     * @return
     */
    public static Long extracKey(String value){
        return Long.parseLong(value.split("::")[0]);
    }

    private static void createTopics(String topic) {

        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 1));

        client.createTopics(topics);
        client.close();
    }
}

