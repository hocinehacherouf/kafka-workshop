package com.kafka.workshop.init.producer;

import avro.shaded.com.google.common.collect.Lists;
import com.kafka.workshop.avro.WorkshopValue;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StreamProducer implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger("com.kafka.workshop.init");

    private static final String topic_in = "split-topic-in";

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, WorkshopValue> producer = new KafkaProducer<>(props);

        try {

            String key;
            WorkshopValue workshopValue;

            for(int i=0; i<500; i++) {

                Thread.sleep(50);
                key = getRandomReference();
                workshopValue = WorkshopValue.newBuilder()
                        .setId(getAlphaNumericString(10))
                        .setReference(key)
                        .setStat(new Integer(i).toString())
                        .setDescription(String.format("description %s", key))
                        .setValid(isValid())
                        .build();

                ProducerRecord<String, WorkshopValue> record = new ProducerRecord<>(topic_in, key, workshopValue);
                if(!workshopValue.getValid()){
                    buildHeader(record, "reason", getAlphaNumericString(20));
                }
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception e) {
                        if (e != null) {
                            logger.error("KAFKA ERROR topic={} partition={} offset={} error={}", metadata.topic(), metadata.partition(), metadata.offset(), e.getMessage());
                        } else {
                            logger.info("KAFKA SUCCESS topic={} partition={} offset={}", metadata.topic(), metadata.partition(), metadata.offset());
                        }
                    }
                });
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            producer.close();
            System.out.println("MMMMMMMMMMMM = " +Math.random());
            System.exit(0);
        }
    }

    private static boolean isValid(){
        if(Math.random() < 0.99){
            return true;
        }
        return false;
    }

    public static String getRandomReference() {
        Random rand = new Random();
        List<String> givenList = Lists.newArrayList("111111", "222222");
        int randomIndex = rand.nextInt(givenList.size());
        String reference = givenList.get(randomIndex);
        return reference;

    }

    private static String getRandomStat() {
        Random rand = new Random();
        List<String> givenList = Lists.newArrayList("50", "60", "70", "80", "90", "100", "110", "120", "130");

        int randomIndex = rand.nextInt(givenList.size());
        String stat = givenList.get(randomIndex);
        return  stat;
    }

    private static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    private void buildHeader(ProducerRecord<String, WorkshopValue> record, String headerKey, String headerValue) {
        RecordHeader recordHeader = new RecordHeader(headerKey, headerValue.getBytes());
        record.headers().add(recordHeader);
    }

}
