#### This exercise aims at writing a KSTREAM java application that branch a stream in two stream and deduplicates values 

The objective of this exercise is to:  
- read records from split-topic-in topic  
- branch the stream depending on the isValid property  
- on each branch apply the transform() method that allows to call the processor API  

    
#### Start the Kafka cluster
In the KSTREAM directory, start the Kafka Cluster using the docker-compose command  
```docker-compose up -d```  

The cluster is composed of 1 zookeeper, 1 broker and 1 schema-registry  
The docker-compose file contains also a base container that allows to run Kafka commands  

#### Configure the KSTREAM topology  
Follow the TODO instructions in the partial.com.kafka.workshop.SplitStream java classes   
Follow the TODO instructions in the partial.com.kafka.workshop.InvalidWorkshopValueTransformer and solution.com.kafka.workshop.ValidWorkshopValueTransformer java classes  
####Start the Stream application
Start the application  

#### Consume example records
Use the com.kafka.workshop.init.consumer.SplitStreamConsumerApplication class to consumer records from split-valid-topic-out and split-invalid-topic-out topics     
Check that filtering is made correctly    

#### Produce example records
Use the com.kafka.workshop.init.producer.SplitStreamProducerApplication class to produce records to split-topic-in topic 
